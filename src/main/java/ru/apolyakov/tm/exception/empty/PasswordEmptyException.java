package ru.apolyakov.tm.exception.empty;

import org.jetbrains.annotations.NotNull;
import ru.apolyakov.tm.exception.AbstractException;

public final class PasswordEmptyException extends AbstractException {

    @NotNull
    private static final String MESSAGE = "Error! Password is empty.";

    public PasswordEmptyException() {
        super(MESSAGE);
    }

}
