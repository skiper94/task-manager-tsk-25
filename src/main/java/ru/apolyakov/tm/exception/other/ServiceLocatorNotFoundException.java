package ru.apolyakov.tm.exception.other;

import org.jetbrains.annotations.NotNull;
import ru.apolyakov.tm.exception.AbstractException;

public class ServiceLocatorNotFoundException extends AbstractException {

    @NotNull
    private static final String MESSAGE = "Service locator not found!";

    public ServiceLocatorNotFoundException() {
        super(MESSAGE);
    }

}
