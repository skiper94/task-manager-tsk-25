package ru.apolyakov.tm;

import org.jetbrains.annotations.NotNull;
import ru.apolyakov.tm.bootstrap.Bootstrap;

public class App {
    public static void main(String[] args) {
        @NotNull final Bootstrap bootstrap = new Bootstrap();
        bootstrap.run(args);
    }
}
