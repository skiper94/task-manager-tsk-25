package ru.apolyakov.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.apolyakov.tm.api.service.IPropertyService;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class PropertyService implements IPropertyService {

    @NotNull public static String FILE_NAME = "app.properties";

    @NotNull private static final Properties properties = new Properties();

    @NotNull private static final String PASSWORD_SECRET = "password.secret";

    @NotNull private static final String PASSWORD_SECRET_DEFAULT = "2021";

    @NotNull private static final String PASSWORD_ITERATION = "password.iteration";

    @NotNull private static final String PASSWORD_ITERATION_DEFAULT = "2031";

    @NotNull private static final String APP_VER = "app.ver";

    @NotNull private static final String APP_VER_DEFAULT = "NO AVAILABLE";

    @SneakyThrows
    public PropertyService() {
        getPropertiesFromFile();
    }

    @Override
    @NotNull
    public String getPasswordSecret(){
        if (System.getenv().containsKey(PASSWORD_SECRET)) return System.getenv(PASSWORD_SECRET);
        if (System.getProperties().containsKey(PASSWORD_SECRET))
            return System.getProperties().getProperty(PASSWORD_SECRET);
        return properties.getProperty(PASSWORD_SECRET, PASSWORD_SECRET_DEFAULT);
    }

    @Override
    @NotNull
    public Integer getPasswordIteration(){
        if (System.getenv().containsKey(PASSWORD_ITERATION)) return Integer.parseInt(System.getenv(PASSWORD_ITERATION));
        if (System.getProperties().containsKey(PASSWORD_ITERATION))
            return Integer.parseInt(System.getProperties().getProperty(PASSWORD_ITERATION));
        return Integer.parseInt(properties.getProperty(PASSWORD_ITERATION, PASSWORD_ITERATION_DEFAULT));
    }

    @Override
    @NotNull
    public String getAppVer() {
        if (System.getenv().containsKey(APP_VER)) return System.getenv(APP_VER);
        if (System.getProperties().containsKey(APP_VER))
            return System.getProperties().getProperty(APP_VER);
        return properties.getProperty(APP_VER, APP_VER_DEFAULT);
    }

    @Override
    public final void getPropertiesFromFile() throws IOException {
        @Nullable final InputStream inputStream = ClassLoader.getSystemResourceAsStream(FILE_NAME);
        if (inputStream == null) return;;
        properties.load(inputStream);
        inputStream.close();
    }

}
