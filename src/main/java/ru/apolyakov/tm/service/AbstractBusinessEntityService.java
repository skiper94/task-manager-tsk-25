package ru.apolyakov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.apolyakov.tm.api.repository.IBusinessEntityRepository;
import ru.apolyakov.tm.api.service.IAuthService;
import ru.apolyakov.tm.api.service.IBusinessEntityService;
import ru.apolyakov.tm.enumerated.Status;
import ru.apolyakov.tm.exception.empty.IdEmptyException;
import ru.apolyakov.tm.exception.empty.NameEmptyException;
import ru.apolyakov.tm.exception.incorrect.IndexIncorrectException;
import ru.apolyakov.tm.exception.security.AccessDeniedNotAuthorizedException;
import ru.apolyakov.tm.model.AbstractBusinessEntity;
import ru.apolyakov.tm.util.ValidationUtil;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;

public abstract class AbstractBusinessEntityService<E extends AbstractBusinessEntity> extends AbstractService<E> implements IBusinessEntityService<E> {

    @NotNull
    protected final IAuthService authService;

    @NotNull
    protected final IBusinessEntityRepository<E> repository;

    public AbstractBusinessEntityService(@NotNull final IBusinessEntityRepository<E> repository, @NotNull final IAuthService authService) {
        super(repository);
        this.repository = repository;
        this.authService = authService;
    }

    @Override
    public void add(@NotNull final String name, @Nullable final String description) {
        if (ValidationUtil.isEmptyStr(name)) throw new NameEmptyException();
        if (ValidationUtil.isEmptyStr(authService.getUserId())) throw new AccessDeniedNotAuthorizedException();
        repository.add(authService.getUserId(), name, description);
    }

    @Override
    public void clear() {
        if (ValidationUtil.isEmptyStr(authService.getUserId())) throw new AccessDeniedNotAuthorizedException();
        if (authService.isPrivilegedUser()) repository.clear();
        else repository.clearLimitByUser(authService.getUserId());
    }

    @Nullable
    @Override
    public E completeById(@NotNull final String id) {
        return updateStatusById(id, Status.COMPLETED);
    }

    @Nullable
    @Override
    public E completeByIndex(final int index) {
        return updateStatusByIndex(index, Status.COMPLETED);
    }

    @Nullable
    @Override
    public E completeByName(@NotNull final String name) {
        return updateStatusByName(name, Status.COMPLETED);
    }

    @NotNull
    @Override
    public List<E> findAll() {
        if (ValidationUtil.isEmptyStr(authService.getUserId())) throw new AccessDeniedNotAuthorizedException();
        if (authService.isPrivilegedUser()) return repository.findAll();
        return repository.findAllLimitByUser(authService.getUserId());
    }

    @NotNull
    @Override
    public List<E> findAll(@NotNull final Comparator<E> comparator) {
        if (ValidationUtil.isEmptyStr(authService.getUserId())) throw new AccessDeniedNotAuthorizedException();
        if (authService.isPrivilegedUser()) return repository.findAll(comparator);
        return repository.findAllLimitByUser(authService.getUserId(), comparator);
    }

    @Nullable
    @Override
    public E findOneById(@Nullable final String id) {
        if (ValidationUtil.isEmptyStr(id)) throw new IdEmptyException();
        if (ValidationUtil.isEmptyStr(authService.getUserId())) throw new AccessDeniedNotAuthorizedException();
        if (authService.isPrivilegedUser()) return repository.findOneById(id);
        return repository.findOneByIdLimitByUser(authService.getUserId(), id);
    }

    @Nullable
    @Override
    public E findOneByIndex(final int index) {
        if (ValidationUtil.isInvalidListIndex(index, getSize())) throw new IndexIncorrectException(index + 1);
        if (ValidationUtil.isEmptyStr(authService.getUserId())) throw new AccessDeniedNotAuthorizedException();
        if (authService.isPrivilegedUser()) return repository.findOneByIndex(index);
        return repository.findOneByIndexLimitByUser(authService.getUserId(), index);
    }

    @Nullable
    @Override
    public E findOneByName(@NotNull final String name) {
        if (ValidationUtil.isEmptyStr(name)) throw new NameEmptyException();
        if (ValidationUtil.isEmptyStr(authService.getUserId())) throw new AccessDeniedNotAuthorizedException();
        if (authService.isPrivilegedUser()) return repository.findOneByName(name);
        return repository.findOneByNameLimitByUser(authService.getUserId(), name);
    }

    @Nullable
    @Override
    public E removeOneById(@NotNull final String id) {
        if (ValidationUtil.isEmptyStr(id)) throw new IdEmptyException();
        if (ValidationUtil.isEmptyStr(authService.getUserId())) throw new AccessDeniedNotAuthorizedException();
        if (authService.isPrivilegedUser()) return repository.removeOneById(id);
        return repository.removeOneByIdLimitByUser(authService.getUserId(), id);
    }

    @Nullable
    @Override
    public E removeOneByIndex(final int index) {
        if (ValidationUtil.isInvalidListIndex(index, getSize())) throw new IndexIncorrectException(index + 1);
        if (ValidationUtil.isEmptyStr(authService.getUserId())) throw new AccessDeniedNotAuthorizedException();
        if (authService.isPrivilegedUser()) return repository.removeOneByIndex(index);
        return repository.removeOneByIndexLimitByUser(authService.getUserId(), index);
    }

    @Nullable
    @Override
    public E removeOneByName(@NotNull final String name) {
        if (ValidationUtil.isEmptyStr(name)) throw new NameEmptyException();
        if (ValidationUtil.isEmptyStr(authService.getUserId())) throw new AccessDeniedNotAuthorizedException();
        if (authService.isPrivilegedUser()) return repository.removeOneByName(name);
        return repository.removeOneByNameLimitByUser(authService.getUserId(), name);
    }

    @Nullable
    @Override
    public E updateById(@NotNull final String id, @NotNull final String name, @Nullable final String description) {
        if (ValidationUtil.isEmptyStr(id)) throw new IdEmptyException();
        if (ValidationUtil.isEmptyStr(name)) throw new NameEmptyException();
        if (ValidationUtil.isEmptyStr(authService.getUserId())) throw new AccessDeniedNotAuthorizedException();
        if (authService.isPrivilegedUser()) return repository.update(id, name, description);
        return repository.updateOneLimitByUser(authService.getUserId(), id, name, description);
    }

    @Nullable
    @Override
    public E updateByIndex(final int index, @NotNull final String name, @Nullable final String description) {
        if (ValidationUtil.isInvalidListIndex(index, getSize())) throw new IndexIncorrectException(index + 1);
        @Nullable final String id = Optional.ofNullable(getId(index)).orElse(null);
        if (id == null) return null;
        return updateById(id, name, description);
    }

    @Nullable
    @Override
    public E updateStatusById(@NotNull final String id, @NotNull final Status status) {
        if (ValidationUtil.isEmptyStr(id)) throw new IdEmptyException();
        if (ValidationUtil.isEmptyStr(authService.getUserId())) throw new AccessDeniedNotAuthorizedException();
        if (authService.isPrivilegedUser()) return repository.updateStatusById(id, status);
        return repository.updateStatusByIdLimitByUser(authService.getUserId(), id, status);
    }

    @Nullable
    @Override
    public E updateStatusByIndex(final int index, @NotNull final Status status) {
        if (ValidationUtil.isInvalidListIndex(index, getSize())) throw new IndexIncorrectException(index + 1);
        if (ValidationUtil.isEmptyStr(authService.getUserId())) throw new AccessDeniedNotAuthorizedException();
        if (authService.isPrivilegedUser()) return repository.updateStatusByIndex(index, status);
        return repository.updateStatusByIndexLimitByUser(authService.getUserId(), index, status);
    }

    @Nullable
    @Override
    public E updateStatusByName(@NotNull final String name, @NotNull final Status status) {
        if (ValidationUtil.isEmptyStr(name)) throw new NameEmptyException();
        if (ValidationUtil.isEmptyStr(authService.getUserId())) throw new AccessDeniedNotAuthorizedException();
        if (authService.isPrivilegedUser()) return repository.updateStatusByName(name, status);
        return repository.updateStatusByNameLimitByUser(authService.getUserId(), name, status);
    }

    @Nullable
    @Override
    public E startById(@NotNull final String id) {
        return updateStatusById(id, Status.IN_PROGRESS);
    }

    @Nullable
    @Override
    public E startByIndex(final int index) {
        return updateStatusByIndex(index, Status.IN_PROGRESS);
    }

    @Nullable
    @Override
    public E startByName(@NotNull final String name) {
        return updateStatusByName(name, Status.IN_PROGRESS);
    }

    @Nullable
    @Override
    public String getId(final int index) {
        if (ValidationUtil.isEmptyStr(authService.getUserId())) throw new AccessDeniedNotAuthorizedException();
        if (authService.isPrivilegedUser()) return repository.getId(index);
        return repository.getIdLimitByUser(authService.getUserId(), index);
    }

    @Nullable
    @Override
    public String getId(@NotNull final String name) {
        if (ValidationUtil.isEmptyStr(authService.getUserId())) throw new AccessDeniedNotAuthorizedException();
        if (authService.isPrivilegedUser()) return repository.getId(name);
        return repository.getIdLimitByUser(authService.getUserId(), name);
    }

    @Override
    public int getSize() {
        if (ValidationUtil.isEmptyStr(authService.getUserId())) throw new AccessDeniedNotAuthorizedException();
        if (authService.isPrivilegedUser()) return repository.getSize();
        else return repository.getSizeLimitByUser(authService.getUserId());
    }

    @Override
    public boolean isEmpty() {
        if (ValidationUtil.isEmptyStr(authService.getUserId())) throw new AccessDeniedNotAuthorizedException();
        if (authService.isPrivilegedUser()) return repository.isEmpty();
        return repository.isEmptyLimitByUser(authService.getUserId());
    }

    @Override
    public boolean isNotFoundById(@NotNull final String id) {
        if (ValidationUtil.isEmptyStr(authService.getUserId())) throw new AccessDeniedNotAuthorizedException();
        if (authService.isPrivilegedUser()) return repository.isNotFoundById(id);
        return repository.isNotFoundByIdLimitByUser(authService.getUserId(), id);
    }

}
