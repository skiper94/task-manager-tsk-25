package ru.apolyakov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.apolyakov.tm.enumerated.Role;
import ru.apolyakov.tm.model.User;

import static ru.apolyakov.tm.util.TerminalUtil.readLine;

public final class UserShowByLoginCommand extends AbstractUserCommand {

    @NotNull
    private static final String NAME = "user-show-by-login";

    @NotNull
    private static final String DESCRIPTION = "Showing user by login";

    @NotNull
    @Override
    public final String getCommand() {
        return NAME;
    }

    @NotNull
    @Override
    public final String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public final Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

    @Override
    public final void execute() {
        @NotNull final String login = readLine(ENTER_LOGIN);
        @Nullable final User user = getUserService().findByLogin(login);
        showUser(user);
    }

}
