package ru.apolyakov.tm.command.task;

import org.jetbrains.annotations.NotNull;

import static ru.apolyakov.tm.util.TerminalUtil.readLine;

public final class TaskCompleteByNameCommand extends AbstractTaskCommand {

    @NotNull
    private final static String NAME = "task-finish-by-name";

    @NotNull
    private final static String DESCRIPTION = "Finishing task by name";

    @NotNull
    @Override
    public final String getCommand() {
        return NAME;
    }

    @NotNull
    @Override
    public final String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public final void execute() {
        @NotNull final String name = readLine(NAME_INPUT);
        throwExceptionIfNull(getTaskService().completeByName(name));
    }

}
