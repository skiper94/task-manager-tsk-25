package ru.apolyakov.tm.command.task;

import org.jetbrains.annotations.NotNull;

import static ru.apolyakov.tm.util.TerminalUtil.readLine;

public final class TaskAddToProjectCommand extends AbstractTaskCommand {

    @NotNull
    private final static String NAME = "task-bind-to-project";

    @NotNull
    private final static String DESCRIPTION = "Adding task to a project(ID to ID)";

    @NotNull
    @Override
    public final String getCommand() {
        return NAME;
    }

    @NotNull
    @Override
    public final String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public final void execute() {
        @NotNull final String projectId = readLine(PROJECT_ID_INPUT);
        @NotNull final String taskId = readLine(TASK_ID_INPUT);
        throwExceptionIfNull(getProjectTaskService().addTaskToProject(projectId, taskId));
    }

}
