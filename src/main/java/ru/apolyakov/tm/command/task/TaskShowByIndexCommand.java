package ru.apolyakov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.apolyakov.tm.model.Task;

import static ru.apolyakov.tm.util.TerminalUtil.readNumber;

public final class TaskShowByIndexCommand extends AbstractTaskCommand {

    @NotNull
    private final static String NAME = "task-show-by-index";

    @NotNull
    private final static String DESCRIPTION = "Showing task by index";

    @NotNull
    @Override
    public final String getCommand() {
        return NAME;
    }

    @NotNull
    @Override
    public final String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public final void execute() {
        final int index = readNumber(INDEX_INPUT);
        @Nullable final Task task = getTaskService().findOneByIndex(index - 1);
        showTask(task);
    }

}
