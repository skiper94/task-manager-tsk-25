package ru.apolyakov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.apolyakov.tm.util.TerminalUtil;

public final class ProjectClearCommand extends AbstractProjectCommand {

    @NotNull
    private final static String NAME = "projects-clear";

    @NotNull
    private final static String DESCRIPTION = "Deleting all projects";

    @NotNull
    @Override
    public final String getCommand() {
        return NAME;
    }

    @NotNull
    @Override
    public final String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public final void execute() {
        final int count = getProjectService().getSize();
        getProjectTaskService().clearProjects();
        TerminalUtil.printLinesWithEmptyLine(count + " projects removed");
    }

}
