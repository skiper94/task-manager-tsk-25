package ru.apolyakov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.apolyakov.tm.model.Project;
import ru.apolyakov.tm.util.TerminalUtil;

public final class ProjectShowByIndexCommand extends AbstractProjectCommand {

    @NotNull
    private final static String NAME = "project-show-by-index";

    @NotNull
    private final static String DESCRIPTION = "Showing project by index";

    @NotNull
    @Override
    public final String getCommand() {
        return NAME;
    }

    @NotNull
    @Override
    public final String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public final void execute() {
        final int index = TerminalUtil.readNumber(INDEX_INPUT);
        @Nullable final Project project = getProjectService().findOneByIndex(index - 1);
        showProject(project);
    }

}
