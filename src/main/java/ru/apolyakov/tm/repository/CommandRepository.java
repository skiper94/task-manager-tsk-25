package ru.apolyakov.tm.repository;

import org.jetbrains.annotations.NotNull;
import ru.apolyakov.tm.api.repository.ICommandRepository;
import ru.apolyakov.tm.command.AbstractCommand;

import java.util.LinkedHashMap;
import java.util.Map;

public final class CommandRepository implements ICommandRepository {

    @NotNull
    private final Map<String, AbstractCommand> arguments = new LinkedHashMap<>();

    @NotNull
    private final Map<String, AbstractCommand> commands = new LinkedHashMap<>();

    @NotNull
    public final Map<String, AbstractCommand> getArguments() {
        return arguments;
    }

    @NotNull
    public final Map<String, AbstractCommand> getCommands() {
        return commands;
    }
}
