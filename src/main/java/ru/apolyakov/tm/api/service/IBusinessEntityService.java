package ru.apolyakov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.apolyakov.tm.api.IService;
import ru.apolyakov.tm.enumerated.Status;
import ru.apolyakov.tm.model.AbstractBusinessEntity;

import java.util.Comparator;
import java.util.List;

public interface IBusinessEntityService<E extends AbstractBusinessEntity> extends IService<E> {

    void add(@NotNull String name, @Nullable String description);

    void clear();

    @Nullable E completeById(@NotNull String id);

    @Nullable E completeByIndex(int index);

    @Nullable E completeByName(@NotNull String name);

    @NotNull
    List<E> findAll(@NotNull Comparator<E> comparator);

    @Nullable
    E findOneByIndex(int index);

    @Nullable
    E findOneByName(@NotNull String name);

    @Nullable
    String getId(int index);

    @Nullable
    String getId(@NotNull String name);

    boolean isNotFoundById(@NotNull String id);

    @Nullable
    E removeOneByIndex(int index);

    @Nullable
    E removeOneByName(@NotNull String name);

    @Nullable
    E startById(@NotNull String id);

    @Nullable
    E startByIndex(int index);

    @Nullable
    E startByName(@NotNull String name);

    @Nullable
    E updateById(@NotNull String id, @NotNull String name, @Nullable String description);

    @Nullable
    E updateByIndex(int index, @NotNull String name, @Nullable String description);

    @Nullable
    E updateStatusById(@NotNull String id, @NotNull Status status);

    @Nullable
    E updateStatusByIndex(int index, @NotNull Status status);

    @Nullable
    E updateStatusByName(@NotNull String name, @NotNull Status status);

}
