package ru.apolyakov.tm.api;

import org.jetbrains.annotations.NotNull;
import ru.apolyakov.tm.api.service.*;

public interface ServiceLocator {

    @NotNull IAuthService getAuthService();

    @NotNull ICommandService getCommandService();

    @NotNull IProjectService getProjectService();

    @NotNull IProjectTaskService getProjectTaskService();

    @NotNull ITaskService getTaskService();

    @NotNull IUserService getUserService();

    @NotNull IPropertyService getPropertyService();
}
