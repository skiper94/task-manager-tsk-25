package ru.apolyakov.tm.api.repository;

import ru.apolyakov.tm.model.Project;

public interface IProjectRepository extends IBusinessEntityRepository<Project> {

}
